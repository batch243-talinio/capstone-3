const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")

	// Route for log-in
		router.post("/login", userController.loginUser);

	// Route for user details
		router.post("/details", auth.verify ,userController.getProfile);

	// Route for user profile
		router.get("/profile", auth.verify ,userController.profileDetails);

	// Update user role (admin)
		router.patch("/updateRole/:userId", auth.verify, userController.updateUserRole);

	// Route for registration
		router.post("/register", userController.checkEmailExists, userController.registerUser);

	// // Check-out/buy product (user)
	// 	router.put("/checkOut/:productId", auth.verify, userController.productQuantity, userController.checkOutProductInsert, userController.checkOutProductUserInsert);

	// // Check-out/buy product from Cart(user)
	// 	router.put("/checkOut/:productId", auth.verify, userController.productQuantity, userController.checkOutProduct, userController.removeCart);

//, userController.removeCart
module.exports = router;