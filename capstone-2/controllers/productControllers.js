const Product = require("../models/Product");
const Users = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// isActive product function
let isActiveProduct = (status) => status == true;

// Check if the new-product to be added already existed (Admin)
module.exports.checkProductExists = (request, response, next) => {
	return Product.find({name: request.body.name}).then(result => {
		// return if(condition) ? t : f
		return result.length > 0 ? response.send(false) : next();
	})
}

// Add new product (Admin)
module.exports.createProduct = (request, response) =>{

	let userData = auth.decode(request.headers.authorization);
	let newProduct = new Product({
		name: 			request.body.name,
		description: 	request.body.description,
		quantity: 		request.body.quantity,
		price: 			request.body.price,
	})

	// return if(condition) ? t : f
	return !userData.isAdmin ? response.send(false) : newProduct.save()
		.then(product => response.send(false))
			.catch(error => response.send(false))
}


// Get all active products
module.exports.getActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => response.send(result))
		.catch(error => response.send(error))
}

// Check out Product - logged in user only - non-Admin
module.exports.checkoutProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productID = request.params.productId;

	return Product.findById(productID).then(result => {
		// let baughtProducts = 'for dynamic or multiple product checkout';
		if(result.quantity <= 0){
			response.send(false);
		}
		let update ={
			quantity : result.quantity - 1
		}

		return Product.findByIdAndUpdate(productID, update, {new: true})
			.then(result => response.send(true)).catch(error => response.send(false))

	}).catch(error => response.send(false))
}


// Get all products
module.exports.getAllProducts = (request, response) => {
	let userData = auth.decode(request.headers.authorization);

	return !userData.isAdmin ? response.send(false) : Product.find({})
		.then(result => response.send(result)).catch(error => response.send(error))
}


// Get one product (All client)
module.exports.getOneProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productID = request.params.productId;
	
	return Product.findById({_id: productID}).then(result => {
		if(!userData.isAdmin) {
			return isActiveProduct(result.isActive) ? response.send(result) : response.send(`You don't have acces to this page!`)
		}

		return response.send(result)

	}).catch(error => response.send(`Product doesn't exist. Please try again.`))
}


// Update a product (Admin)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let updatedProduct = {
		name: 			request.body.name,
		description: 	request.body.description,
		price: 			request.body.price,
		quantity:  		request.body.quantity
	}

	// return if(condition) ? t : f
	return !userData.isAdmin ? response.send(false) :
		Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
			.then(result => response.send(result)).catch(error => response.send(false))

}


// Archive / un-Archive a product (Admin)
module.exports.archiveAndUnarchive = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	return Product.findOne({id : productId})
	 	.then(result => {
	 		let update = { isActive: !result.isActive }
	 		//let message = () => !result.isActive ? `The product is un-Archived successfully.` : `The product is Archived successfully.`

	 		return !userData.isAdmin ? response.send(false) :
				Product.findByIdAndUpdate(productId, update, {new: true})
					.then(result => response.send(true)).catch(error => response.send(false))

	 	}).catch(error => response.send(error))
}


