import {Container, Row} from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Dashboard from '../components/AdminDashboard';

import {useContext} from 'react';
import UserContext from '../UserContext';


export default function Home(){

  const {user} = useContext(UserContext);

	return(
	  <Container>
      <Row>
        {
          (user.isAdmin) 
          ? 
            <Dashboard/>
          :
            <Banner />

        }
      </Row>      
    </Container>
		)
}