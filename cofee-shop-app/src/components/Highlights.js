// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card, Button, CardGroup, Modal} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import React, {useState} from 'react';
import Swal from 'sweetalert2';

export default function Highlights({product}){
	const { _id, name, description, price, quantity, isActive} = product;

	const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

	const userToken = localStorage.getItem(`token`)


	const cardStyle = {
		transitionDuration: "0.1s",
		minHeight: "100%",
		maxHeight: 500,
	  };
	
	  const checkLogIn = () => {
		Swal.fire({
			title: "Please log-in first!",
			icon: "info"
		})
	  }

	  const checkOut = () => {
		//console.log(_id)
		fetch(`${process.env.REACT_APP_URI}/products/checkout/${_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization : `Bearer ${localStorage.getItem(`token`)}`
            }
        }).then(response => response.json())
		.then(data =>{
			(!data) 
			? 
			Swal.fire({
				title: "Checked out failed!",
				icon: "error",
				text: data
			})
			:
			Swal.fire({
				title: "Checked out successfully!",
				icon: "success",
				text: data.quantity
			})
		})

		handleClose()
	  }

	const addToCart = (event) => {
		Swal.fire({
			title: "Add to cart succcessfullly!",
			icon: "success",
		})
		handleClose()
	}


	return(
		<Col className="col-lg-4 col-md-6 col-12 mt-4">
			<CardGroup>
				<Card style={cardStyle} className="cardHighlight blog-preview">
				<Card.Img variant="top" alt = "example" src="https://www.myweekly.co.uk/wp-content/uploads/sites/9/2018/06/iStock-814684194-coffee-x.jpg" />
					<Card.Body>
						<Card.Title>{name}</Card.Title>
							<Card.Text>{description}</Card.Text>
					</Card.Body>
					<div className='mb-2'>
						{
							(userToken === null) 
							? <Link to = "/login"><Button onClick = {checkLogIn} variant="primary">More info...</Button></Link>

							: <Button onClick = {handleShow} variant="primary">More info...</Button>
						}
					</div>
				</Card>
			</CardGroup>


			<Modal show={show} onHide={handleClose} size="md" centered className="vh-100" scrollable>
                <Modal.Body>
						<Card className="cardHighlight">
						<Card.Img variant="top" alt = "example" src="https://www.myweekly.co.uk/wp-content/uploads/sites/9/2018/06/iStock-814684194-coffee-x.jpg" />
							<Card.Body>
								<Card.Title>{name}</Card.Title>
									<Card.Text>{description}</Card.Text>
							</Card.Body>
						</Card>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" onClick = {checkOut}>Order now</Button>
                    <Button variant="primary" type = "submit" onClick = {function(){addToCart()}}>Add to Cart</Button>
                </Modal.Footer>
            </Modal>
		</Col>
	)
}








{/*
<Col className="col-lg-4 col-md-6 col-12 mt-4 mh-50">
			<CardGroup>
				<Card style={cardStyle}>
				<Card.Img variant="top" alt = "example" src="https://www.myweekly.co.uk/wp-content/uploads/sites/9/2018/06/iStock-814684194-coffee-x.jpg" />
					<Card.Body>
						<Card.Title>{name}</Card.Title>
							<Card.Text>{description}</Card.Text>
						<div className='text-end'>
							<Button variant="primary">more info...</Button>
						</div>
					</Card.Body>
				</Card>
			</CardGroup>
		</Col>

*/}
